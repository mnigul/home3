import java.util.LinkedList;

public class LongStack {
   LinkedList<Long> mag;

   public static void main (String[] argum) {

      /* Tests
      System.out.println(interpret("40 20 -7 + /"));
      System.out.println(interpret("1 -15 4 7 8 - + * +"));
      System.out.println(interpret("1 -15 4 7 8 s + * x"));
      */
   }

   LongStack() {
      mag = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack ret = new LongStack();
      ret.mag = (LinkedList<Long>) mag.clone();
      return ret;
   }

   public boolean stEmpty() {
      return mag.isEmpty() ? true : false;
   }

   public void push (long a) {
      mag.push(a);
   }

   public long pop() {
      return mag.pop();
   } // pop

   public void op (String s) {
      char op = s.charAt(0);
      long nr1 = mag.remove(1);
      long nr2 = mag.remove(0);
      switch (op) {
         default:
            throw new RuntimeException("Illegal operation " + op + " valid operations are + - * /");
         case '+':
            push(nr1 + nr2);
            break;
         case '-':
            push(nr1 - nr2);
            break;
         case '*':
            push(nr1 * nr2);
            break;
         case '/':
            push(nr1 / nr2);
            break;
      }
   }
  
   public long tos() {
      return mag.peek();
   }

   @Override
   public boolean equals (Object o) {
      if (o instanceof LongStack) {
         LinkedList<Long> temp = ((LongStack) o).mag;
         if (mag.size() == temp.size()) {
            for (int i = 0; i < mag.size(); i++) {
               if (mag.get(i) != temp.get(i)) {
                  return false;
               }
            }
            return true;
         }
      }
      return false;
   }

   @Override
   public String toString() {
      StringBuffer ret = new StringBuffer();
      for (int i = mag.size() - 1; i >= 0; i--) {
         ret.append(mag.get(i));
         if (i > 0) {
            ret.append(' ');
         }
      }
      return ret.toString();
   }

   public static long interpret (String pol) {
      if (pol == null || pol.length() == 0)
         throw new RuntimeException("Expression is empty");

      LongStack temp = new LongStack();
      String[] elements = pol.trim().split("\\s+");
      int i = 0;
      int j = 0;

      for (String element : elements) {
         try {
            temp.push(Integer.valueOf(element));
            i++;
         } catch (NumberFormatException e) {
            if (!element.equals("+") && !element.equals("-") && !element.equals("*") && !element.equals("/"))
               throw new RuntimeException("'" + element + "' is not a valid symbol.");
            else if (temp.stEmpty())
               throw new RuntimeException("Operation can not be the first element of expression.");
            else if (temp.mag.size() < 2)
               throw new RuntimeException("Stack needs more elements.");
            temp.op(element);
            j++;
         }
      }
      if (i - 1 != j)
         throw new RuntimeException("Stack is out of balance.");
      return temp.pop();
   }

}

